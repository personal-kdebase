/*  This file is part of the KDE project
    Copyright (C) 2007 Matthias Kretz <kretz@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.

*/

#ifndef MYGRAPHICSSCENE_H
#define MYGRAPHICSSCENE_H

#include <QtCore/QObject>
#include <QtGui/QGraphicsLineItem>
#include <QtGui/QGraphicsScene>
#include <QtGui/QGraphicsSceneMouseEvent>
#include "widgetrectitem.h"

class MyGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
    public:
        MyGraphicsScene(QObject *parent)
            : QGraphicsScene(parent),  m_lineItem(0), m_view(0)
        {}

        void setView(QGraphicsView *v) { m_view = v; }

    protected:
        void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
        void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);

    private:
        QGraphicsLineItem *m_lineItem;
        WidgetRectItem *m_startItem;
        QGraphicsView *m_view;
};

#endif // MYGRAPHICSSCENE_H
