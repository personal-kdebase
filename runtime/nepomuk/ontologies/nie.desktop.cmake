[Desktop Entry]
Version=1.0
Name=Nepomuk Information Element Core Ontology
Comment=Core ontology for the Nepomuk Information Element framework which provides a unified vocabulary for describing native resources available on the desktop.
URL=http://www.semanticdesktop.org/ontologies/2007/01/19/nie#
Path=${DATA_INSTALL_DIR}/nepomuk/ontologies/nie.trig
MimeType=application/x-trig
Type=Data
