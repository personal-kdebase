This file lists TODO items for the compositing code.

See file COMPOSITE_HOWTO for setting up kwin_composite.
See file HACKING for details on developing KWin, including building
    the kwin_composite branch.
See effects/howto.* for a HOWTO on writting effects.
See documentation in source (mainly in scene.cpp) for description
    of the design of the compositing framework.

TODO
=================================

* = not done, will be either done by me, or should be at least discussed first with me
+ = not done, I don't plan on doing it that soon
    - in other words, these should be the best ones for you if you want to help
! = like +, but they should be relatively simple
    - in other words, these should be the best if you want to get started with the code
/ = work in progress
? = should it be done?
% = should be probably done later, during cleanups and preparations for being stable


KDE 4.0 TODO
=================================

- effects (to do)
    - add generic support to use effect only for certain window types [Seli]

- window-specific rules need to apply to unmanaged windows as well (where applicable)
- some window-specific rules also related to compositing
    - opacity

/ handling of window pixmaps for unmapped windows [Seli]
  - config option?

- shm mode needs support for more data formats than GL_BGRA

- check what works with XRender

- documentation

- disable application effects when kwin can provide them, e.g. shadow/transparency


General TODO
=================================

+ suspend repaints if not visible or dpms is active
    - there is already 'overlay_visible', but it still polls the mouse in such case
    - it should turn off the timer

? wait for decoration repaints
    - it is sometimes visible that the window contents are painted first and the decoration
        only afterwards with a small delay
?       - this has been already greatly improved by r632378, so it's maybe not worth it anymore
    - maybe posted paint events need to be processed immediatelly, or maybe the compositing
        code should not update the window until the decoration is finished painting

% paint throttling
    - there's 5ms grace period per repaint to avoid overloading the system with just compositing
        and not letting the system do anything else - check and evaluate 

* handle properly stacking order of deleted windows for showing in effects

* handle properly deleted windows that reappear (windowReadded() function?)

% during screensaving, do no let non-screensaver windows show above screensaver
    - kdesktop_lock watches for such things and raises again, but there's a small gap

% nvidia drivers by default use fake refresh rates as a workaround for some X limitations
    - see the DynamicTwinView section in nvidia README
    - this makes KWin repaint at a different rate than it should

/ handling of window pixmap for unmapped windows
    - currently it's kept around after a window is unmapped
*       - but it's still discarded on e.g. resize - how to solve this?
*   - windows could be unmapped but quickly mapped back when a live thumbnail is needed

* window grouping is not implemented for unmanaged windows (used e.g. by DimInactive)

% clean up and sort out shortcuts so that they don't conflict and make sense

? hidden previews currently needs input shape extension, otherwise the window can possibly interfere
    - not very likely though, so is this worth bothering at all?


OpenGL TODO
=================================

/ Check/make it work with other gfx cards

% Xgl support
    - should work
    - in SceneGL::Texture::findTarget() there is a hack that makes KWin work with XGL
        without requiring KWin to be built against the libGL version that Compiz is
        built against
%       - that may not be necessary somewhen later
    - interesting observation: normally run glxgears performs somewhat poorly compared
        to normal nvidia mode, changing glxgears to be override-redirect (i.e. no reparenting)
        however makes glxgears to be even faster than without any compositing at all,
        in this case however kwin's redrawing speed drops somewhat

% AIGLX support
    - should work

/ GL_ARB_texture_rectangle vs GL_ARB_texture_non_power_of_two
%   - works; bugs in tfp_mode with power_of_two textures
        - ati (others?): power_of_two windows are drawn white unless non-tfp_mode
            is forced in findTextureTarget()

% bindTexture() optimize copied areas
    - right now bindTexture() updates every damaged area and resets the window damage
    - it might make things faster to update only areas that need to be repainted
        and keep the rest damaged until needed

% clipping optimization
    - like XRender code has paintTransformedScreen(), avoid painting parts that are
        obscured (makes a difference with many windows open)
    - http://lists.kde.org/?l=kwin&m=116585618111882&w=2

! shm mode needs support for more data formats than GL_BGRA
    - http://www.xfree86.org/current/glTexImage2D.3.html
    - this now works for 16bpp, but maybe not 15bpp or less
    - endian issues?

+ support for __GL_YIELD=NOTHING and LIBGL_ALWAYS_INDIRECT should be possibly less hacky
    - or at least not hardcoded (although, does that matter?)
    - (http://lists.kde.org/?l=kwin&m=116439615124838&w=2)
    - (http://lists.freedesktop.org/archives/xorg/2006-December/020323.html)


XRender TODO
==============================

+ SceneXrender::paintTransformedScreen() doesn't handle properly extending of painted area
    in window's pre-paint - see the transformedShape() comment
    - and currently seems to be generally broken


Effects framework TODO
==============================

* more notification functions for effects are needed
    - currently there are only very few notification functions (windowAdded, windowActivated,...)
!   - window state changes
    ? more

/ support for grabbing input
    - during some more complicated effects, input (at least mouse) should be disabled,
        because currently there is no way to do input redirection

+ EffectWindow should be completely opaque when kept as the only API for effects
    - no inlines, etc.

+ EffectsHandler should not have virtuals but should instead use an internal object
    for communication with KWin core, so that adding new functions doesn't break BC all the time

+ API for tabbox for effects should be cleaned up

* check Scene::updateTimeDiff() - should the time be 0 or 1?

% post calls are probably not necessary by now (http://lists.kde.org/?t=117770818100003&r=1&w=2)

% consider using http://lists.kde.org/?l=kwin&m=118094888517415&w=2 for notification functions

% API cleanups and making sure it will stay backwards compatible

Effects TODO
===============================

! add reloadConfig() to effects
    - currently changing the global animation speed in the kcm does not change effect

+ minimize/shade effects
    - to replace the ones from KWin core
/       - minimizing
            - check support for it and the avoid_animation flag
+       - shading
            - shading will probably need special support

/ zoom effect
    - enlarge a portion of the screen
    - Should it react to meta+wheel?

+ logout effect
*   - should be triggered by ksmserver somehow

+ effects to replace widget effects (the ones in the Effects tab in "kcmshell style")

/ showfps effect
    - should also detect kwin being idle - it probably should detect in pre-paint that the only
        damage is its own area and avoid damaging for the next round in post-paint

/ scalein
+    - should scale in from e.g. 80%, not 0%, to just "pop up"

+ effect for drawing attention to windows demanding attention
    - Rays? Ripples?

* DimInactive flickers when switching between windows (temporarily no window becomes active)

- focus effect (windows waves a little when it gets focus)

+ shadow
+   - follow the shape of the window (currently is disabled completely using the hasOwnShadow() hack)

+ window thumbnails directly in panel
    - i.e. the panel (taskbar?) could show window previews directly, without tooltips


Performance
==================

+ various caching - there are many things that usually don't change and could gain few fps (is it worth it?)
    - Workspace::performCompositing()
    - WindowQuadList could perhaps cache quads split by contents/decoration (for select())

+ avoiding X roundtrips each compositing pass
    - right now it at least always polls the mouse position and the current stacking order
